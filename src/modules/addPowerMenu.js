import GObject from 'gi://GObject';
import GLib from 'gi://GLib'
import St from 'gi://St';

import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

export const PowerMenuIndicator = GObject.registerClass(
  class PowerMenuIndicator extends PanelMenu.Button {
    _init() {
      super._init(0.0, _('My Shiny Indicator'));

      this.menuIcon = new St.Icon({
        icon_name: 'system-shutdown-symbolic',
        style_class: 'system-status-icon'
      });

      this.add_child(this.menuIcon);

      this.suspendButton = new PopupMenu.PopupImageMenuItem(_('Suspend'), 'weather-clear-night-symbolic');
      this.suspendButton.connect('activate', () => {
        GLib.spawn_command_line_async('systemctl suspend');
      });
      this.menu.addMenuItem(this.suspendButton);

      this.powerOffButton = new PopupMenu.PopupImageMenuItem(_('Power Off…'), 'system-shutdown-symbolic');
      this.powerOffButton.connect('activate', () => {
        GLib.spawn_command_line_async('gnome-session-quit --power-off');
      });
      this.menu.addMenuItem(this.powerOffButton);

      this.rebootButton = new PopupMenu.PopupImageMenuItem(_('Restart…'), 'system-reboot-symbolic');
      this.rebootButton.connect('activate', () => {
        GLib.spawn_command_line_async('gnome-session-quit --reboot');
      });
      this.menu.addMenuItem(this.rebootButton);

      this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

      this.logoutButton = new PopupMenu.PopupImageMenuItem(_('Log Out…'), 'system-log-out-symbolic');
      this.logoutButton.connect('activate', () => {
        GLib.spawn_command_line_async('gnome-session-quit --logout');
      });
      this.menu.addMenuItem(this.logoutButton);
    }
  });