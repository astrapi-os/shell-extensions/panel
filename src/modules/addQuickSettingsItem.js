import GObject from 'gi://GObject';
import GLib from 'gi://GLib'

import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';
import { QuickSettingsItem, QuickMenuToggle } from 'resource:///org/gnome/shell/ui/quickSettings.js';

export const SettingsMenuToogle = GObject.registerClass(
    class SettingsMenuToogle extends QuickMenuToggle {
      _init(indicator) {
        super._init({
          title: _('Settings'),
          iconName: 'video-display-symbolic',
          toggleMode: false,
        });

        this.menu.setHeader('preferences-system-privacy-symbolic', _('Settings'));

        // Add the menu title
        // this.menuTitle = new PopupMenu.PopupImageMenuItem(_('Settings ---'), 'video-display-symbolic', {
        //   reactive: false,
        //   style_class: 'selectLabel'
        // });
        // this.menuTitle._icon.icon_size = 24;
        // this.menu.addMenuItem(this.menuTitle);
  
        // // Add a separator
        // this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
  
        this.settingsButton = new PopupMenu.PopupMenuItem(_("Settings"));
        this.settingsButton.connect('activate', () => {
            GLib.spawn_command_line_async('gnome-control-center');
          });
        this.menu.addMenuItem(this.settingsButton);

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this.extensionsButton = new PopupMenu.PopupMenuItem(_("Extensions"));
        this.extensionsButton.connect('activate', () => {
            GLib.spawn_command_line_async('extension-manager');
          });
        this.menu.addMenuItem(this.extensionsButton);
  
        this.tweaksButton = new PopupMenu.PopupMenuItem(_("Gnome Tweaks"));
        this.tweaksButton.connect('activate', () => {
            GLib.spawn_command_line_async('gnome-tweaks');
          });
        this.menu.addMenuItem(this.tweaksButton);
  
        // const item4 = new PopupMenu.PopupMenuItem(_("Desktop Config Editor"));
        // this.menu.addMenuItem(item4);

      }
    }
  )

  export const SettingsMenuIndicator = GObject.registerClass(
    class SettingsMenuIndicator extends QuickSettings.SystemIndicator {
        _init(extensionObject) {
            super._init();
            this._indicator = this._addIndicator();
        }
    });
