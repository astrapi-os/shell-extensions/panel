import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import { QuickSettingsItem } from 'resource:///org/gnome/shell/ui/quickSettings.js';

export const hideQuickSettingsItems = new GObject.registerClass(
    class hideQuickSettingsItems extends QuickSettingsItem {
      _init(param, status) {
        this._system = param
        if (status == "enable")
          this._system._systemItem.visible = false;
        else
        this._system._systemItem.visible = true;
      }
    }
  )