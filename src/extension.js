import GLib from 'gi://GLib'
import { Extension, gettext as _ } from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import { loggin } from "./modules/loggin.js"
import { PowerMenuIndicator } from "./modules/addPowerMenu.js"
import { hideQuickSettingsItems } from "./modules/hideQuickSettingsItems.js"
import { SettingsMenuIndicator, SettingsMenuToogle } from "./modules/addQuickSettingsItem.js"

let sourceId = null;

export default class PanelExtension extends Extension {
    enable() {
        loggin("enable")

        this.powerMenuIndicator = new PowerMenuIndicator(Main.panel);
        Main.panel._addToPanelBox('powerMenu', this.powerMenuIndicator, -1, Main.panel._leftBox);

        this.initTimeout = GLib.timeout_add_seconds(
            GLib.PRIORITY_DEFAULT,
            1,
            () => {
                this.getBattery((proxy, icon) => {
                    if (proxy.Type == 0) {
                        icon.hide()
                    }
                })
                return GLib.SOURCE_REMOVE
            }
        )

        if (Main.panel.statusArea.quickSettings._system)
            console.log("modifySystemItem");
        else
            console.log("queueModifySystemItem");
        this._queueModifySystemItem("enable")

        this.settingsMenuIndicator = new SettingsMenuIndicator(this);
        this.settingsMenuIndicator.quickSettingsItems.push(new SettingsMenuToogle(this));

        Main.panel.statusArea.quickSettings.addExternalIndicator(this.settingsMenuIndicator);
    }

    disable() {
        loggin("disable")
        this.powerMenuIndicator.destroy();
        this.powerMenuIndicator = null;

        this.settingsMenuIndicator.quickSettingsItems.forEach(item => item.destroy());
        this.settingsMenuIndicator.quickSettingsItems = [];

        this.settingsMenuIndicator.destroy();
        this.settingsMenuIndicator = null;

        this.initTimeout = GLib.timeout_add_seconds(
            GLib.PRIORITY_DEFAULT,
            1,
            () => {
                this.getBattery((proxy, icon) => {
                    if (proxy.Type == 0) {
                        icon.show()
                    }
                })
                return GLib.SOURCE_REMOVE
            }
        )

        if (Main.panel.statusArea.quickSettings._system)
            console.log("modifySystemItem");
        else
            console.log("queueModifySystemItem");
        this._queueModifySystemItem("disable")
    }

    getBattery(callback) {
        let system = Main.panel.statusArea.quickSettings._system
        console.log("---")
        console.log("DEBUG: " + system)
        console.log("---")
        if (system && system._systemItem._powerToggle) {
            callback(system._systemItem._powerToggle._proxy, system)
        }
    }

    _modifySystemItem(status) {
        const modifiedMenu = new hideQuickSettingsItems(Main.panel.statusArea.quickSettings._system, status);
    }

    _queueModifySystemItem(status) {
        sourceId = GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
            if (!Main.panel.statusArea.quickSettings._system)
                return GLib.SOURCE_CONTINUE;

            this._modifySystemItem(status);
            return GLib.SOURCE_REMOVE;
        });
    }
}

